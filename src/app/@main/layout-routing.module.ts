import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ButterflyComponent } from '../@core/photobooth/butterfly/butterfly.component';
import { GlassComponent } from '../@core/photobooth/glass/glass.component';
import { HatComponent } from '../@core/photobooth/hat/hat.component';
import { ThankyouComponent } from '../thankyou/thankyou.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { LayoutComponent } from './layout.component';
import { Lobby2Component } from './lobby2/lobby2.component';

const routes: Routes = [
  { path:'', redirectTo:'lobby', pathMatch:'full' },
  { path: 'lobby', loadChildren: ()=>import('../@core/lobby/lobby.module').then(m=>m.LobbyModule)},
  { path: 'auditorium', loadChildren: ()=>import('../@core/auditorium/auditorium.module').then(m=>m.AuditoriumModule) },
  { path: 'exhibition', loadChildren: ()=>import('../@core/exhibition-hall/exhibition-hall.module').then(m=>m.ExhibitionHallModule) },
  { path: 'lounge', loadChildren: ()=>import('../@core/networking-lounge/networking-lounge.module').then(m=>m.NetworkingLoungeModule) },
  { path: 'gamezone', loadChildren: ()=>import('../@core/game-zone/game-zone.module').then(m=>m.GameZoneModule) },
  { path: 'photobooth', loadChildren: ()=>import('../@core/photobooth/photobooth.module').then(m=>m.PhotoboothModule) },
  { path: 'helpdesk', loadChildren: ()=>import('../@core/helpdesk/helpdesk.module').then(m=>m.HelpdeskModule) },
  { path: 'signaturewall', loadChildren: ()=>import('../@core/signaturewall/signaturewall.module').then(m=>m.SignaturewallModule) },
  { path: 'appointments', component:AppointmentsComponent },
  { path: 'lobby2', component:Lobby2Component },
  { path: 'thank-you', component:ThankyouComponent },
  { path: 'hat', component:HatComponent },
  { path: 'butterfly', component:ButterflyComponent },
  { path: 'glass', component:GlassComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }

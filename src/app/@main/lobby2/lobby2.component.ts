import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { localService } from 'src/app/services/data.service';

@Component({
  selector: 'app-lobby2',
  templateUrl: './lobby2.component.html',
  styleUrls: ['./lobby2.component.scss']
})
export class Lobby2Component implements OnInit {

  constructor(private router: Router,private _ls:localService) { }

  ngOnInit(): void {
//     var countDownDate = new Date("Feb 26, 2022 00:30:00").getTime();

// var x = setInterval(function() {

//   var now = new Date().getTime();
    
//   var distance = countDownDate - now;
    
//   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//   var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
//   document.getElementById("demo").innerHTML = days + "D " + hours + "H "
//   + minutes + "M " + seconds + "S ";
    
//   if (distance < 0) {
//     clearInterval(x);
//     document.getElementById("demo").innerHTML = "EXPIRED";
//   }
// }, 1000);
  }
  

  postPath(data){
    this._ls.stepUpAnalytics(`click_`+data);
    this.router.navigateByUrl('/'+data);
  }

}

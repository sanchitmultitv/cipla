import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { BriefcaseModalComponent } from './briefcase-modal/briefcase-modal.component';
import { LeaderboardModalComponent } from './leaderboard-modal/leaderboard-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppointmentsComponent } from './appointments/appointments.component';
import { QuizModule } from 'src/app/@main/shared/quiz/quiz.module';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { Lobby2Component } from './lobby2/lobby2.component';
@NgModule({
  declarations: [LayoutComponent, MenuComponent, BriefcaseModalComponent, LeaderboardModalComponent,AppointmentsComponent, Lobby2Component],
  imports: [
    CommonModule,
    RouterModule,FormsModule,
    LayoutRoutingModule,ReactiveFormsModule,PdfmodalModule,QuizModule
  ]
})
export class LayoutModule { }

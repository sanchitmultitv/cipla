import { Component, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-pdfmodal',
  templateUrl: './pdfmodal.component.html',
  styleUrls: ['./pdfmodal.component.scss']
})
export class PdfmodalComponent implements OnInit {
  @Input() rName:any;
  agenda: any;
  constructor(private _ds:DataService,private sanitiser: DomSanitizer) { }
  audiObj:any = {};
  ngOnInit(): void {

    this._ds.getActiveAuditorium(205115).subscribe((res:any)=>{
      console.log('res',res);
      this.audiObj = res.result?.[0];
      let age
      age = res.result?.[0].agenda;
      // alert(this.agenda)
      this.agenda = this.sanitiser.bypassSecurityTrustResourceUrl(age);
      // this.playStream(res.result?.[0].stream,this.poster);
    });

  
  //   $('#iframe_feedback').on("load", ()=> {
  //     console.log('iframe loaded'); //replace with code to hide loader
  // });
  }
  closeModal(){
    $("#pdf_modal").modal('hide');
    $("#win_modal").modal('hide');
    $("#quiz_modal").modal('hide');
  }
}

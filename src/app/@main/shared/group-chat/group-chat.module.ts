import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupChatComponent } from './group-chat.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GroupChatComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [GroupChatComponent]
})
export class GroupChatModule { }

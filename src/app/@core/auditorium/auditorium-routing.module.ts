import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditoriumComponent } from './auditorium.component';

const routes: Routes = [
  {
    path:'', 
    component: AuditoriumComponent,
    children: [
      {path:'', redirectTo:'home', pathMatch:'full'},
      {path:'home', loadChildren:()=>import('./auditorium-home/auditorium-home.module').then(m=>m.AuditoriumHomeModule)},
      {path:'full/:id', loadChildren:()=>import('./full-auditorium/full-auditorium.module').then(m=>m.FullAuditoriumModule)},
      {path:'stage/:id', loadChildren:()=>import('./stage-auditorium/stage-auditorium.module').then(m=>m.StageAuditoriumModule)},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditoriumRoutingModule { }

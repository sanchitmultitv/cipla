import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LobbyRoutingModule } from './lobby-routing.module';
import { LobbyComponent } from './lobby.component';
import { HttpClientModule } from '@angular/common/http';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';


@NgModule({
  declarations: [LobbyComponent],
  imports: [
    CommonModule,
    LobbyRoutingModule,
    HttpClientModule,
    PdfmodalModule
  ]
})
export class LobbyModule { }

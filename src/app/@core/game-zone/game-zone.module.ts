import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameZoneRoutingModule } from './game-zone-routing.module';
import { GameZoneComponent } from './game-zone.component';


@NgModule({
  declarations: [GameZoneComponent],
  imports: [
    CommonModule,
    GameZoneRoutingModule
  ]
})
export class GameZoneModule { }

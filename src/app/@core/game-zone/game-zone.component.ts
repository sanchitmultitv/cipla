import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { DataService, localService } from 'src/app/services/data.service';
declare var $:any;

@Component({
  selector: 'app-game-zone',
  templateUrl: './game-zone.component.html',
  styleUrls: ['./game-zone.component.scss']
})
export class GameZoneComponent implements OnInit {
  gameData = [];
  bgImg;
  iframe_main_URL:any;
  dat:any;
  constructor(private router: Router,private _ls:localService,private _ds:DataService, private render: Renderer2, private elementRef: ElementRef, private _auth:AuthService) { }

  ngOnInit(): void {
    this.getSettingSection();
    this.getGamezonedata();
  }
  getSettingSection(){
    this._ds.getSettingSection().subscribe(res=>{
      this.bgImg = res['bgImages']['gamezone'];
      console.log(this.bgImg)
    });
    // this._auth.settingItems$.subscribe(items => {
    //   if(items.length){
    //     this.bgImg = items[0]['bgImages']['gamezone'];
    //   }
    // });
  }
  getGamezonedata(){
    this._ds.getGamezonedata().subscribe((res:any)=>{
      this.gameData = res.result;
    });
  }
  pointerMethod(item,title){
    // console.log(item)
    //alert(title)
    this._ls.stepUpAnalytics(`click_`+title);
    let data = JSON.parse(localStorage.getItem('virtual'));
    
    this.iframe_main_URL = item.iframe_url+data.mobile
    $('#open_game_modal').modal('show');
    document.getElementById("popupData").childNodes.forEach((ele:any) => {
    if(ele.id){
      
      document.getElementById(ele.id).remove();
    }
    }); 
    var iframe:any = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="600px";
    iframe.id="randomid";
    iframe.setAttribute("src", this.iframe_main_URL);
    document.getElementById("popupData").appendChild(iframe);
    // const iframe:any = this.render.createElement('iframe');
    // this.render.setAttribute(this.elementRef.nativeElement, 'src', item.iframe_url)
    // this.render.appendChild(this.pdata, iframe);
    // return iframe;  
  }
  closeModal(){
  //   $('#open_game_modal').on('hidden.bs.modal', function () {
  //     player.stopVideo();
  //    // or jQuery
  //    $('#playerID').get(0).stopVideo();
  //    // or remove video url
  //    $('playerID').attr('src', '');
  // })
  //   $('#open_game_modal').on('hidden.bs.modal', function () {
  //     $("#open_game_modal").attr("src", $("#open_game_modal").attr("src"));
  // });
  $("#open_game_modal").on('hidden.bs.modal', function (e) {
    $("#open_game_modal iframe").attr("src", $("#open_game_modal iframe").attr("src"));
});
    $('#open_game_modal').modal('hide');
    
  }
  postPath(data){
    this.router.navigateByUrl('/'+data);
  }
}

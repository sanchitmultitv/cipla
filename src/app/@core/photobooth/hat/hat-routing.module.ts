import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HatComponent } from './hat.component';

const routes: Routes = [{ path: '', component: HatComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HatRoutingModule { }

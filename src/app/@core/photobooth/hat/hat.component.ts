import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-hat',
  templateUrl: './hat.component.html',
  styleUrls: ['./hat.component.scss']
})
export class HatComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    $("#armodal").modal('show');
    $("#armodal2").modal('hide');
    $("#armodal3").modal('hide');
  }
  closeModalNew(){
    $("#armodal").modal('hide');
  }
  postPath(data){
    this.router.navigateByUrl('/'+data);
  }
  openPhotobooth(){
    $("#armodal").modal('show');
  }
}

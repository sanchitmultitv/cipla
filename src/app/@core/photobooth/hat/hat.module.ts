import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HatRoutingModule } from './hat-routing.module';
import { HatComponent } from './hat.component';


@NgModule({
  declarations: [HatComponent],
  imports: [
    CommonModule,
    HatRoutingModule
  ]
})
export class HatModule { }

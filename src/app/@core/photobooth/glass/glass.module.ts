import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlassRoutingModule } from './glass-routing.module';
import { GlassComponent } from './glass.component';


@NgModule({
  declarations: [GlassComponent],
  imports: [
    CommonModule,
    GlassRoutingModule
  ]
})
export class GlassModule { }

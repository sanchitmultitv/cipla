import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-glass',
  templateUrl: './glass.component.html',
  styleUrls: ['./glass.component.scss']
})
export class GlassComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    $("#armodal2").modal('show');
    $("#armodal").modal('hide');
    $("#armodal3").modal('hide');
  }
  closeModalNew2(){
    $("#armodal2").modal('hide');
  }
  postPath(data){
    this.router.navigateByUrl('/'+data);
  }
  openPhotobooth(){
    $("#armodal2").modal('show');
  }
}

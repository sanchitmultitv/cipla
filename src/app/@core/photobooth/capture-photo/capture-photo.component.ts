// import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter, HostListener } from '@angular/core';
// import { toBase64String } from '@angular/compiler/src/output/source_map';
// import { ChatService } from 'src/app/services/chat.service';
// import { DataService } from 'src/app/services/data.service';
// declare var $: any;
// @Component({
//   selector: 'app-capture-photo',
//   templateUrl: './capture-photo.component.html',
//   styleUrls: ['../photobooth.component.scss']
// })
// export class CapturePhotoComponent implements OnInit {

//   videoWidth = 0;
//   videoHeight = 0;
//   showImage = false;
//   liveMsg = false; 
//   camstream: any;
//   img;
//   dataurl;
//   bgimage = 'assets/event/photobooth_files/photobooth.png';
  
//   selfieImg = this.bgimage;
//   morning:boolean;
//   constructor(private renderer: Renderer2, private _ds: DataService, private chat: ChatService) { }
//   @ViewChild('video', { static: true }) videoElement: ElementRef;
//   @ViewChild('canvas', { static: true }) canvas: ElementRef;
//   @Output('myOutputVal') myOutputVal = new EventEmitter();

//   ngOnInit(): void {
    
//     let modal = document.getElementById("capture_photo_modal");
//     window.onclick = (event) => {
//       if (event.target == modal) {
//         this.closeModal();
//       }
//     }
//   }

//   openCapturePhotoModal() {
//     $('#capture_photo_modal').modal('show');
//     this.startCamera();
//   }
//   constraints = {
//     facingMode: { exact: 'environment' },
//     video: {
//       width: { ideal: 720 },
//       height: { ideal: 480 }
//     }
//   };

//   startCamera() {
//     if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
//       navigator.mediaDevices.getUserMedia(this.constraints).then(this.attachVideo.bind(this)).catch(this.handleError);
//     } else {
//       alert('Sorry, camera not available.');
//     }

//   }
//   handleError(error) {
//     console.log('Error: ', error);
//   }
//   stream: any;
//   attachVideo(stream) {
//     this.camstream = stream;
//     this.renderer.setProperty(this.videoElement.nativeElement, 'srcObject', this.camstream);
//     this.renderer.listen(this.videoElement.nativeElement, 'play', (event) => {
//       this.videoHeight = this.videoElement.nativeElement.videoHeight;
//       this.videoWidth = this.videoElement.nativeElement.videoWidth;
//     });
//   }
//   stopStream() {
//     if (null != this.camstream) {

//       var track = this.camstream.getTracks()[0];

//       track.stop();
//       this.videoElement.nativeElement.load();

//       this.camstream = null;
//     }
//   }
//   capture() {
//     this.showImage = true;
//     let canvas: any = document.getElementById("canvas");
//     let vid: any = document.getElementById("vid");
//     let selfie: any = document.getElementById('selfie');
//     this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
//     this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight - 50);
//     this.canvas.nativeElement.getContext('2d').drawImage(this.videoElement.nativeElement, 55,0, selfie.width-110, selfie.height);
//     let context = canvas.getContext('2d');
//     context.lineWidth = 1;
//     var bgImg = new Image();
//     bgImg.setAttribute('crossOrigin', 'anonymous');
//     bgImg.src = 'assets/event/photobooth_files/photobooth.png';
//     context.drawImage(bgImg, 0, 0, selfie.width, selfie.height);
//     this.dataurl = canvas.toDataURL('mime');
//     this.img = canvas.toDataURL("image/png");
//   }

//   uploadVideo() {
//     let user_id = JSON.parse(localStorage.getItem('virtual')).id;
//     let user_name = JSON.parse(localStorage.getItem('virtual')).name;
//     const formData = new FormData();
//     formData.append('user_id', user_id);
//     formData.append('user_name', user_name);
//     formData.append('image', this.img);
//     this._ds.uploadCapturePic(formData).subscribe(res => {
//     });
//   }

//   showCapture() {
//     $('.capturePhoto').modal('show');
//     this.startCamera();
//   }


//   closeModal(){
//     this.stopStream();
//     $("#capture_photo_modal").modal("hide");
//   }
//   reload() {
//     this.showImage = false;
//     this.startCamera();
//   }
//   @HostListener('keydown', ['$event']) onKeyDown(key) {
//     if (key.keyCode === 27) {
//       this.closeModal();
//     }
//   }
// }


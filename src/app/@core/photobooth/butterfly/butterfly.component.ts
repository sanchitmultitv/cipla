import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-butterfly',
  templateUrl: './butterfly.component.html',
  styleUrls: ['./butterfly.component.scss']
})
export class ButterflyComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    $("#armodal3").modal('show');
    $("#armodal2").modal('hide');
    $("#armodal").modal('hide');
  }
  closeModalNew3(){
    $("#armodal3").modal('hide');
  }
  postPath(data){
    this.router.navigateByUrl('/'+data);
  }
  openPhotobooth(){
    $("#armodal3").modal('show')
  }
}
